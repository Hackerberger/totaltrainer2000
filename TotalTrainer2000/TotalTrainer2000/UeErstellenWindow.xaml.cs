﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TotalTrainer2000
{
    /// <summary>
    /// Interaktionslogik für UeErstellenWindow.xaml
    /// </summary>
    public partial class UeErstellenWindow : Window
    {

        bool fensterSchl = false;

        public UeErstellenWindow()
        {
            InitializeComponent();
        }

        //Wenn Fenster geschlossen wird, HAuptfenster wieder zeigen.
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (fensterSchl == true)
            {
                App.Current.MainWindow.Show();
            }
            else
            {
                MessageBoxResult result =
              MessageBox.Show(
                "Alle Daten gehen verloren!\n Willst du wirklich die Erstellung abbrechen?",
                "Abbrechen",
                MessageBoxButton.YesNo,
                MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    // If user doesn't want to close, cancel closure
                    e.Cancel = true;
                }
                else
                {
                    App.Current.MainWindow.Show();
                }
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(textB_Name.Text) == true || string.IsNullOrEmpty(textB_Beschr.Text) == true)
            {
                MessageBox.Show(
                "Alle Felder die mit * gekennzeichnet sind, müssen ausgefüllt werden!",
                "Übung konnte nicht erstellt werden",
                MessageBoxButton.OK,
                MessageBoxImage.Warning);
            }
            else
            {
                try
                {
                    Uebung ue = new Uebung(textB_Name.Text, textB_Beschr.Text);
                    var window = App.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                    window.EigeneUe.Add(ue);
                    fensterSchl = true;
                    this.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(
                    "Überprüfe deine Eingabe!",
                    "Übung konnte nicht erstellt werden",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);

                }
            }          
        }
    }
}
