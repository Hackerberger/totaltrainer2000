﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TotalTrainer2000
{
    /// <summary>
    /// Interaktionslogik für TrErstellenWindow.xaml
    /// </summary>
    public partial class TrErstellenWindow : Window
    {

        bool fensterSchl = false;
        ObservableCollection<Uebung> hinzugefUe;
        Training.swgk swgk;
        int dauer;

        public TrErstellenWindow(List<Uebung> ue)
        {
            InitializeComponent();

            hinzugefUe = new ObservableCollection<Uebung>();

            listBox_Uebungen.ItemsSource = ue;
            listBox_Erstellen.ItemsSource = hinzugefUe;
        }

        //Wenn Fenster geschlossen wird, HAuptfenster wieder zeigen.
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (fensterSchl == true)
            {
                App.Current.MainWindow.Show();
            }
            else
            {
                MessageBoxResult result =
              MessageBox.Show(
                "Alle Daten gehen verloren!\n Willst du wirklich die Erstellung abbrechen?",
                "Abbrechen",
                MessageBoxButton.YesNo,
                MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    // If user doesn't want to close, cancel closure
                    e.Cancel = true;
                }
                else
                {
                    App.Current.MainWindow.Show();
                }
            }

        }


        private void button_Erstellen_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(textB_Name.Text) == true || hinzugefUe.Count() == 0)
            {
                MessageBox.Show(
                "Alle Felder die mit * gekennzeichnet sind, müssen ausgefüllt werden!",
                "Übung konnte nicht erstellt werden",
                MessageBoxButton.OK,
                MessageBoxImage.Warning);
            }
            else if (textB_Name.Text == "Pause")
                MessageBox.Show(
                "Name darf nicht Pause sein!",
                "Übung konnte nicht erstellt werden",
                MessageBoxButton.OK,
                MessageBoxImage.Warning);
            else
            {
                try
                {
                    if (rBt1.IsChecked == true)
                        swgk = Training.swgk.leicht;
                    else if(rBt2.IsChecked == true)
                        swgk = Training.swgk.mittel;
                    else if (rBt3.IsChecked == true)
                        swgk = Training.swgk.schwer;
                    else
                        swgk = Training.swgk.egal;

                    try
                    {
                        dauer = Convert.ToInt32(textB_Dauer.Text);
                    }
                    catch
                    {
                        dauer = 0;
                    }

                    Training tr = new Training(textB_Name.Text,hinzugefUe.ToList<Uebung>(),textB_Beschr.Text,dauer, swgk);
                    var window = App.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                    window.EigeneTr.Add(tr);

                    fensterSchl = true;
                    this.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(
                    "Überprüfe deine Eingabe!",
                    "Übung konnte nicht erstellt werden",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);

                }
            }
        }

        private void button_Hinzufuegen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Uebung u = (Uebung)listBox_Uebungen.SelectedItem;
                if(u.Name != "Pause")
                    u.Anzahl = Convert.ToInt32(textB_Wiederholungen.Text);

                hinzugefUe.Add(u);
            }
            catch (System.FormatException)
            {
                MessageBox.Show(
               "Wiederholungen wurden falsch angegeben",
               "Fehler",
               MessageBoxButton.OK,
               MessageBoxImage.Warning);
            }
            catch (System.NullReferenceException)
            {
             MessageBox.Show(
               "Es wurde keine Übung ausgewählt!",
               "Fehler",
               MessageBoxButton.OK,
               MessageBoxImage.Warning);
            }

            

        }
    }
}
