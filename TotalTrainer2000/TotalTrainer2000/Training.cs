﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotalTrainer2000
{


    [Serializable]
    public class Training
    {
        public Training()
        {

        }
        public Training(string n,List<Uebung> l)
        {
            Name = n;
            Uebungen = l;
        }
        public Training(string n,List<Uebung> l, string b, int d, swgk schw) : this(n,l)
        {
            Beschreibung = b;
            Dauer = d;
            Schwierigkeit = schw;
        }

        public enum swgk {egal = 0, leicht, mittel, schwer}

        public string Name { get; set; }
        public List<Uebung> Uebungen { get; set; }
        public string Beschreibung { get; set; }
        public int Dauer { get; set; }
        public swgk Schwierigkeit { get; set; }

        public string ganzesTr { get { return Name + ", " + Schwierigkeit +", "+ Dauer + " Minuten"; } }
    }
}
