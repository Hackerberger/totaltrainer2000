﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotalTrainer2000
{
    [Serializable]
    public class Pause : Uebung
    {
        public Pause() : base("Pause", "Pause") { }
        public Pause(int d) : base("Pause","Pause")
        {
            Dauer = d;
        }

        public int Dauer { get; set; }
        public override string ganzeUe { get { return Name; } }
    }
}
