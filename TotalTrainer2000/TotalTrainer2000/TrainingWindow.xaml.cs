﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TotalTrainer2000
{
    /// <summary>
    /// Interaktionslogik für TrainingWindow.xaml
    /// </summary>
    public partial class TrainingWindow : Window
    {
        Training tr;
        ObservableQueue<Uebung> ablauf;
        Uebung letzteUe;
        DispatcherTimer timer;
        DateTime timerZeit;
        bool fensterSchl = false;

        public TrainingWindow(Training tr)
        {
            InitializeComponent();

            this.tr = tr;
            ablauf = new ObservableQueue<Uebung>(this.tr.Uebungen);
            listBox_Abl.ItemsSource = ablauf;
            textBlock_TrainingName.Text = tr.Name;

            //Timer
            timerZeit = new DateTime();
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 1);

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timerZeit = timerZeit.AddSeconds(1);
            textBLock_Timer.Text = timerZeit.ToString("mm:ss");
        }


        //Wenn Fenster geschlossen wird, HAuptfenster wieder zeigen.
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(fensterSchl == true)
            {
                App.Current.MainWindow.Show();
            }
            else
            {
                MessageBoxResult result =
              MessageBox.Show(
                "Aufgeben ist KEINE Option! \n Willst du wirklich aufhören?",
                "Abbrechen",
                MessageBoxButton.YesNo,
                MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    // If user doesn't want to close, cancel closure
                    e.Cancel = true;
                }
                else
                {
                    App.Current.MainWindow.Show();
                }
            }
            
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if ((string)button_Start.Content == "Start")
            {
                timer.Start();
                button_Start.Content = "Nächste Übung";
            }
            else if (ablauf.Count()<2)
            {
                button_Start.Content = "Training beenden";
                listBox_Abl.Visibility = Visibility.Collapsed;
            }
               
            



            //Ende triggern
            if(ablauf.Count() > 0)
            {
                letzteUe = ablauf.Dequeue();
                textBlock_AktUe.Text = letzteUe.ganzeUe;
                if(letzteUe.Name == "Pause")
                {
                    textBLock_Timer.Foreground = new SolidColorBrush(Colors.RoyalBlue);
                    textBlock_AktUe.Foreground = new SolidColorBrush(Colors.RoyalBlue);
                    timer.Stop();
                }
                else
                {
                    textBLock_Timer.Foreground = new SolidColorBrush(Colors.White);
                    textBlock_AktUe.Foreground = new SolidColorBrush(Colors.IndianRed);
                    timer.Start();
                }
            }
            else
            {
                timer.Stop();
                MessageBox.Show("Training " + tr.Name + " abgeschlossen! Zeit: " + timerZeit.ToString("mm:ss"));
                fensterSchl = true;
                this.Close();
            }
        }
    }
}
