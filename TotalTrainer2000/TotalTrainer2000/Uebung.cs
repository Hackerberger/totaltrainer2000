﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotalTrainer2000
{
    [Serializable]
    public class Uebung
    {
        public Uebung() { }
        public Uebung(string n,string b)
        {
            Name = n;
            Beschreibung = b;
        }
        public Uebung(string n, string b,int a) : this(n, b)
        {
            Anzahl = a;
        }


        public string Name { get; set; }
        public string Beschreibung { get; set; }
        public int Anzahl { get; set; }

        public virtual string ganzeUe { get { return Anzahl + "x " + Name; } }
    }
}
