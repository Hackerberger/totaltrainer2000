﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace TotalTrainer2000
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Trainingslisten
        ObservableCollection<Training> ganzkoerperTr = new ObservableCollection<Training>();
        ObservableCollection<Training> armeTr = new ObservableCollection<Training>();
        ObservableCollection<Training> koerpermitteTr = new ObservableCollection<Training>();
        ObservableCollection<Training> beineTr = new ObservableCollection<Training>();
        private ObservableCollection<Training> eigeneTr;

        public ObservableCollection<Training> EigeneTr { get { return eigeneTr; } set { eigeneTr = value; } }

        //Uebungslisten
        ObservableCollection<Uebung> armeUe = new ObservableCollection<Uebung>();
        ObservableCollection<Uebung> koerpermitteUe = new ObservableCollection<Uebung>();
        ObservableCollection<Uebung> beineUe = new ObservableCollection<Uebung>();
        private ObservableCollection<Uebung> eigeneUe;

        public ObservableCollection<Uebung> EigeneUe { get { return eigeneUe; } set { eigeneUe = value; } }

        public MainWindow()
        {
            InitializeComponent();

            StandardErstellen();

            //Eigene Trainings und Übungen laden
            EigeneSachenLaden();
            

            //Binding
            listBox_GanzkoerperTr.ItemsSource = ganzkoerperTr;
            listBox_ArmeTr.ItemsSource = armeTr;
            listBox_KoerpermitteTr.ItemsSource = koerpermitteTr;
            listBox_BeineTr.ItemsSource = beineTr;
            listBox_PersonalTraining.ItemsSource = eigeneTr;
            listBox_ArmeUe.ItemsSource = armeUe;
            listBox_KoerpermitteUe.ItemsSource = koerpermitteUe;
            listBox_BeineUe.ItemsSource = beineUe;
            listBox_PersonalUe.ItemsSource = eigeneUe;


        }

        //Zum ein- und ausklappen der Trainings und Übungen
        private void ListeEinklappen(ListBox lb)
        {
            if (lb.Visibility == Visibility.Visible)
                lb.Visibility = Visibility.Collapsed;
            else
                lb.Visibility = Visibility.Visible;
        }
        //Trainingsfenster öffnen
        private void TrWindowOeffnen(Training t)
        {
            try
            {
                TrainingWindow trW = new TrainingWindow(t);
                App.Current.MainWindow.Hide();
                trW.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Training konnte nicht gestartet werden: " + ex.Message, "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            
        }
        //Beschreibung einer Übung öffnen
        private void UeOeffnen(Uebung ue)
        {
            try
            {
                MessageBox.Show("Beschreibung:\n" + ue.Beschreibung, ue.Name);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Übung konnte nicht angezeigt werden: " + ex.Message, "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);

            }

        }


        //Liste einblenden
        private void textBl_ArmeTr_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_ArmeTr);
        }

        private void textBl_GanzkoerperTr_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_GanzkoerperTr);
        }

        private void textBl_KoerpermitteTr_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_KoerpermitteTr);
        }

        private void textBl_BeineTr_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_BeineTr);
        }

        private void textBl_PersonalTraining_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_PersonalTraining);
        }

        private void textBl_ArmeUe_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_ArmeUe);
        }

        private void textBl_KoerpermitteUe_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_KoerpermitteUe);
        }

        private void textBl_BeineUe_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_BeineUe);
        }

        private void textBl_PersonalUe_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ListeEinklappen(listBox_PersonalUe);
        }



        //Aufruf Trainingsfenster
        private void listBox_GanzkoerperTr_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TrWindowOeffnen((Training)listBox_GanzkoerperTr.SelectedItem);
        }

        private void listBox_ArmeTr_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TrWindowOeffnen((Training)listBox_ArmeTr.SelectedItem);
        }

        private void listBox_KoerpermitteTr_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TrWindowOeffnen((Training)listBox_KoerpermitteTr.SelectedItem);
        }

        private void listBox_BeineTr_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TrWindowOeffnen((Training)listBox_BeineTr.SelectedItem);
        }

        private void listBox_PersonalTraining_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TrWindowOeffnen((Training)listBox_PersonalTraining.SelectedItem);
        }

        //Aufruf Übungsbeschreibung
        private void listBox_ArmeUe_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            UeOeffnen((Uebung)listBox_ArmeUe.SelectedItem);
        }

        private void listBox_KoerpermitteUe_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            UeOeffnen((Uebung)listBox_KoerpermitteUe.SelectedItem);
        }

        private void listBox_BeineUe_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            UeOeffnen((Uebung)listBox_BeineUe.SelectedItem);
        }

        private void listBox_PersonalUe_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            UeOeffnen((Uebung)listBox_PersonalUe.SelectedItem);
        }


        //Training & Übung erstellen
        private void grid_EigeneUe_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UeErstellenWindow ueErW = new UeErstellenWindow();
            App.Current.MainWindow.Hide();
            ueErW.Show();
        }

        private void grid_EigenesTr_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            TrErstellenWindow trErW = new TrErstellenWindow(AlleUebungen());
            App.Current.MainWindow.Hide();
            trErW.Show();
        }


        //Daten speichern wenn Fenster geschlossen wird
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {   

            XmlSerializer serializerTr = new XmlSerializer(typeof(List<Training>), new Type[] { typeof(Pause) });


            using (FileStream stream = File.OpenWrite("./../../saves/eigeneTr.xml"))
            {
                serializerTr.Serialize(stream, eigeneTr.ToList<Training>());
            }

            XmlSerializer serializerUe = new XmlSerializer(typeof(List<Uebung>), new Type[] { typeof(Pause) });


            using (FileStream stream = File.OpenWrite("./../../saves/eigeneUe.xml"))
            {
                serializerUe.Serialize(stream, eigeneUe.ToList<Uebung>());
            }
        }


        //Alle Übungen
        private List<Uebung> AlleUebungen()
        {
            List<Uebung> ue = new List<Uebung>();
            ue.Add(new Pause());
            ue.AddRange(eigeneUe);
            ue.AddRange(armeUe);
            ue.AddRange(koerpermitteUe);
            ue.AddRange(beineUe);
            return ue;
        }

        //Erstellen der Standard Trainings & Übungen
        private void StandardErstellen()
        {
            //Übungen
            armeUe.Add(new Uebung("Liegestütze", "normale Liegestütze"));
            armeUe.Add(new Uebung("einarmige Liegestütze", "Liegestütze nach Tiroler Art"));
            armeUe.Add(new Uebung("Klimmzüge", "normale Klimmzüge"));
            koerpermitteUe.Add(new Uebung("Situps", "bis zur Erschöpfung"));
            koerpermitteUe.Add(new Uebung("Legraises", "Immer schön an die Brust"));
            beineUe.Add(new Uebung("Kniebeuge", "Classic!"));
            beineUe.Add(new Uebung("Wadenheben", "Auf einer Kante"));

            //Trainings
            List<Uebung> ue1 = new List<Uebung>();
            ue1.Add(new Uebung("Liegestütze", "normale Liegestütze", 15));
            ue1.Add(new Uebung("Situps", "bis zur Erschöpfung",10));
            ue1.Add(new Uebung("Kniebeuge", "Classic!",40));
            ue1.Add(new Pause(2));
            ue1.Add(new Uebung("einarmige Liegestütze", "Liegestütze nach Tiroler Art",5));
            ue1.Add(new Uebung("Legraises", "Immer schön an die Brust", 8));
            ue1.Add(new Uebung("Wadenheben", "Auf einer Kante",35));
            ganzkoerperTr.Add(new Training("Std Ganzkörper", ue1, "Der goldene Standard", 20, Training.swgk.mittel));

            List<Uebung> ue2 = new List<Uebung>();
            ue2.Add(new Uebung("Situps", "bis zur Erschöpfung",30));
            ue2.Add(new Pause(10));
            ue2.Add(new Uebung("Legraises", "Immer schön an die Brust", 20));
            ue2.Add(new Pause(10));
            ue2.Add(new Uebung("Situps", "bis zur Erschöpfung", 10));
            koerpermitteTr.Add(new Training("Std Koerpermitte", ue2, "Für den Sixpack", 10, Training.swgk.leicht));

            List<Uebung> ue3 = new List<Uebung>();
            ue3.Add(new Uebung("Kniebeuge", "Classic!",30));
            ue3.Add(new Pause(10));
            ue3.Add(new Uebung("Wadenheben", "Auf einer Kante",20));
            ue3.Add(new Pause(10));
            ue3.Add(new Uebung("Kniebeuge", "Classic!", 30));
            ue3.Add(new Uebung("Wadenheben", "Auf einer Kante", 20));
            beineTr.Add(new Training("Std Beine", ue3, "Für die Wadeln", 15, Training.swgk.leicht));

            List<Uebung> ue4 = new List<Uebung>();
            ue3.Add(new Uebung("einarmige Liegestütze", "Liegestütze nach Tiroler Art",5));
            ue3.Add(new Pause(10));
            ue3.Add(new Uebung("einarmige Liegestütze", "Liegestütze nach Tiroler Art", 10));
            ue3.Add(new Pause(10));
            ue3.Add(new Uebung("Liegestütze", "normale Liegestütze",20));
            armeTr.Add(new Training("Std Arme", ue3, "Nur für harte Burschen", 30, Training.swgk.schwer));
        }

        private void EigeneSachenLaden()
        {
            XmlSerializer serializerTr = new XmlSerializer(typeof(List<Training>), new Type[] { typeof(Pause) });
            XmlSerializer serializerUe = new XmlSerializer(typeof(List<Uebung>), new Type[] { typeof(Pause) });

            try
            {
                using (FileStream stream = File.OpenRead("./../../saves/eigeneTr.xml"))
                {
                    List<Training> dezerializedList = (List<Training>)serializerTr.Deserialize(stream);
                    eigeneTr = new ObservableCollection<Training>(dezerializedList);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(
               ex.Message,
               "Eigene Trainings konnten nicht geladen werden",
               MessageBoxButton.OK,
               MessageBoxImage.Error);

                eigeneTr = new ObservableCollection<Training>();
            }

            try
            {
                using (FileStream stream = File.OpenRead("./../../saves/eigeneUe.xml"))
                {
                    List<Uebung> dezerializedList = (List<Uebung>)serializerUe.Deserialize(stream);
                    eigeneUe = new ObservableCollection<Uebung>(dezerializedList);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(
               ex.Message,
               "Eigene Trainings konnten nicht geladen werden",
               MessageBoxButton.OK,
               MessageBoxImage.Error);

                eigeneUe = new ObservableCollection<Uebung>();
            }
            
        }
    }
}
